INTRODUCTION
------------

Allows the creation of custom linkit links, typically based on a Drupal route with optional arguments.

This allows Site Builders to create links to View pages using the view route, e.g. `view.my_view.page_1`. But any Drupal route is supported, allowing Site Builders to provide access to custom Controllers through Linkit.

The route can alternatively be a URI, allowing external links to be easily accessible by editors, and centrally maintained.

REQUIREMENTS
------------

This module requires the following modules:

 * Linkit (https://www.drupal.org/project/linkit)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.
 * Add a "Linkit custom link" Matcher to your Linkit profile.
 * Create one or more Linkit custom links. The label will be matched in the linkit autocomplete widget.
 * Linkit can now provide link substitution for the custom link.
