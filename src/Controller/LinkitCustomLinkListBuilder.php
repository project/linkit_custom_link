<?php

namespace Drupal\linkit_custom_link\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing.
 */
class LinkitCustomLinkListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Link');
    $header['id'] = $this->t('Machine name');
    $header['routename'] = $this->t('Route name');
    $header['routeparameters'] = $this->t('Route parameters');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['routename'] = $entity->getRouteName();
    $row['routeparameters'] = $entity->getRouteParameters();

    return $row + parent::buildRow($entity);
  }

}
