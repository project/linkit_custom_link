<?php

namespace Drupal\linkit_custom_link;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Example entity.
 */
interface LinkitCustomLinkInterface extends ConfigEntityInterface {

  /**
   * Returns the route name.
   *
   * @return string
   *   The route name.
   */
  public function getRouteName();

  /**
   * Returns the route parameters.
   *
   * @return string
   *   The route parameters.
   */
  public function getRouteParameters();

  /**
   * Returns the query.
   *
   * @return string
   *   The query.
   */
  public function getQuery();

  /**
   * Returns the fragment (without #).
   *
   * @return string
   *   The fragment.
   */
  public function getFragment();

}
