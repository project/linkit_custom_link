<?php

namespace Drupal\linkit_custom_link\Plugin\Linkit\Matcher;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\linkit\Plugin\Linkit\Matcher\EntityMatcher;

/**
 * Provides specific linkit matchers for linkit custom links.
 *
 * @Matcher(
 *   id = "linkit_custom_link",
 *   label = @Translation("Linkit custom link"),
 *   target_entity = "linkit_custom_link",
 * )
 */
class LinkitCustomLinkMatcher extends EntityMatcher {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'substitution_type' => 'linkit_custom_link',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['substitution']['substitution_type']['#options'] = [
      'linkit_custom_link' => 'Linkit custom link',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildGroup(EntityInterface $entity) {
    return $this->t('Other links');
  }

  /**
   * Builds the path string used in the suggestion.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The matched entity.
   *
   * @return string
   *   The path for this entity.
   */
  protected function buildPath(EntityInterface $entity) {
    /** @var \Drupal\linkit_custom_link\Entity\LinkitCustomLink $link */
    $link = $entity;

    return $link->getUrl()->toString();
  }

}
