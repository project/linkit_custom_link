<?php

namespace Drupal\linkit_custom_link\Plugin\Linkit\Substitution;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\linkit\SubstitutionInterface;

/**
 * A substitution plugin for the URL of a view.
 *
 * @Substitution(
 *   id = "linkit_custom_link",
 *   label = @Translation("Linkit custom link URL"),
 * )
 */
class LinkitCustomLinkSubstitution extends PluginBase implements SubstitutionInterface {

  /**
   * {@inheritdoc}
   */
  public function getUrl(EntityInterface $entity) {
    /** @var \Drupal\linkit_custom_link\Entity\LinkitCustomLink $link */
    $link = $entity;

    $generated_url = new GeneratedUrl();
    $tags = $link->getCacheTags();
    $generated_url->addCacheableDependency($link);
    $generated_url->setGeneratedUrl($link->getUrl()->toString());

    return $generated_url;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    return $entity_type === 'linkit_custom_link';
  }

}
