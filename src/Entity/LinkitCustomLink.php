<?php

namespace Drupal\linkit_custom_link\Entity;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Url;
use Drupal\linkit_custom_link\LinkitCustomLinkInterface;

/**
 * Defines the entity.
 *
 * @ConfigEntityType(
 *   id = "linkit_custom_link",
 *   label = @Translation("Linkit custom link"),
 *   handlers = {
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\linkit_custom_link\Controller\LinkitCustomLinkListBuilder",
 *     "form" = {
 *       "add" = "Drupal\linkit_custom_link\Form\LinkitCustomLinkForm",
 *       "edit" = "Drupal\linkit_custom_link\Form\LinkitCustomLinkForm",
 *       "delete" = "Drupal\linkit_custom_link\Form\LinkitCustomLinkDeleteForm",
 *     }
 *   },
 *   config_prefix = "link",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "routename" = "routeName",
 *     "routeparameters" = "routeParameters",
 *     "query" = "query",
 *     "fragment" = "fragment",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "routename",
 *     "routeparameters",
 *     "query",
 *     "fragment",
 *   },
 *   links = {
 *     "collection" = "/admin/config/content/linkit_custom_link",
 *     "add-form" = "/admin/config/content/linkit_custom_link/add",
 *     "edit-form" = "/admin/config/content/linkit_custom_link/{linkit_custom_link}",
 *     "delete-form" = "/admin/config/content/linkit_custom_link/{linkit_custom_link}/delete",
 *   }
 * )
 */
class LinkitCustomLink extends ConfigEntityBase implements LinkitCustomLinkInterface, CacheableDependencyInterface {

  /**
   * The Example ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Example label.
   *
   * @var string
   */
  protected $label;

  /**
   * The route name.
   *
   * @var string
   */
  protected $routename;

  /**
   * The route parameters.
   *
   * @var string
   */
  protected $routeparameters;

  /**
   * The query.
   *
   * @var string
   */
  protected $query;

  /**
   * The fragment.
   *
   * @var string
   */
  protected $fragment;

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    return $this->routename;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters() {
    return $this->routeparameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * {@inheritdoc}
   */
  public function getFragment() {
    return $this->fragment;
  }

  /**
   * Returns the URL object represented by the custom link.
   *
   * @return \Drupal\Core\Url
   *   The URL object.
   */
  public function getUrl() {
    // Is route a URL?
    // We assume so if it contains a hash.
    $route_name = $this->getRouteName();

    if ($route_name !== '<front>' && !preg_match('/^[a-z0-9\._]+$/', $route_name)) {
      return Url::fromUri($route_name, $this->buildOptions());
    }

    return Url::fromRoute(
      $route_name,
      $this->buildRouteParameters(),
      $this->buildOptions()
    );
  }

  /**
   * Builds and returns the route parameters.
   *
   * @return array
   *   The route parameters.
   */
  private function buildRouteParameters() {
    $route_parameters = [];

    if ($this->getRouteParameters()) {
      $route_parameters = json_decode($this->getRouteParameters(), JSON_OBJECT_AS_ARRAY);
    }

    return $route_parameters;
  }

  /**
   * Builds and returns the route options.
   *
   * @return array
   *   The options.
   */
  private function buildOptions() {
    $options = [];

    if ($this->getQuery()) {
      $options['query'] = json_decode($this->getQuery(), JSON_OBJECT_AS_ARRAY);
    }

    if ($this->getFragment()) {
      $options['fragment'] = $this->getFragment();
    }

    return $options;
  }

}
