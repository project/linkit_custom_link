<?php

namespace Drupal\linkit_custom_link\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the add and edit forms.
 */
class LinkitCustomLinkForm extends EntityForm {

  /**
   * Constructs an form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\linkit_custom_link\Entity\LinkitCustomLink $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('Label for this custom link.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['routename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Route name'),
      '#maxlength' => 255,
      '#default_value' => $entity->getRouteName(),
      '#description' => $this->t('Typically the Drupal route name for the link. If not, the route is treated as a URI.'),
      '#required' => TRUE,
    ];

    $form['routeparameters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Route parameters'),
      '#maxlength' => 255,
      '#default_value' => $entity->getRouteParameters(),
      '#description' => $this->t('Optional route parameters, in JSON format.'),
    ];

    $form['query'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Query'),
      '#maxlength' => 255,
      '#default_value' => $entity->getQuery(),
      '#description' => $this->t('URL query parameters, in JSON format.'),
    ];

    $form['fragment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fragment'),
      '#maxlength' => 255,
      '#default_value' => $entity->getFragment(),
      '#description' => $this->t('URL fragment (without #).'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $example = $this->entity;
    $status = $example->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Example created.', [
        '%label' => $example->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Example updated.', [
        '%label' => $example->label(),
      ]));
    }

    $form_state->setRedirect('entity.linkit_custom_link.collection');
  }

  /**
   * Helper function to check whether an Example configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('linkit_custom_link')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
